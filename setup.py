from setuptools import setup, find_packages
setup(
    # name of package
    name="snapshot", 
    # packages (directories) to be included
    packages=find_packages(),
    # script entry point
    entry_points={
        "console_scripts": [
            "snapshot = snapshot.snapshot:main",
        ],
    },
    # package dependencies
    install_requires=[
        "psutil==5.9.2",
    ],
    version="0.1",
    author="Captain Jack",
    author_email="captain_jack@gmail.com",
    description="Example of the test application",
    license="MIT")