import argparse
import time
import psutil
import json
import os


class Util:
    def make_snapshot(self):
        keys = ["total", "running", "sleeping", "stopped", "zombie"]
        task_result = dict.fromkeys(keys,0)
        for task in psutil.pids():
            task_result["total"] += 1
            if psutil.Process(task).status() == psutil.STATUS_RUNNING:
                task_result["running"] += 1
            elif psutil.Process(task).status() == psutil.STATUS_SLEEPING:
                task_result["sleeping"] += 1
            elif psutil.Process(task).status() == psutil.STATUS_STOPPED:
                task_result["stopped"] += 1
            elif psutil.Process(task).status() == psutil.STATUS_ZOMBIE:
                task_result["zombie"] += 1 
        
        cpu = [psutil.cpu_times_percent().user,psutil.cpu_times_percent().system,psutil.cpu_times_percent().idle]
        memory = [psutil.virtual_memory().total // 1024, psutil.virtual_memory().free // 1024, 
        psutil.virtual_memory().used // 1024]
        swap = [psutil.swap_memory().total // 1024, psutil.swap_memory().free // 1024, 
        psutil.swap_memory().used // 1024]


        snapshot = {"Tasks": {"total": task_result["total"], "running": task_result["running"], "sleeping": task_result["sleeping"], "stopped": task_result["stopped"], "zombie": task_result["zombie"]},
                    "%CPU": {"user": cpu[0], "system": cpu[1], "idle": cpu[2]},
                    "KiB Mem": {"total": memory[0], "free": memory[1], "used": memory[2]},
                    "KiB Swap": {"total": swap[0], "free": swap[1], "used": swap[2]},
                    "Timestamp": 1624406400}

        return snapshot


def main():
    """Snapshot tool."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="Interval between snapshots in seconds", type=int, default=5)
    parser.add_argument("-f", help="Output file name", default="snapshot.json")
    parser.add_argument("-n", help="Quantity of snapshot to output", default=20)
    args = parser.parse_args()
    util = Util()
    with open(args.f, "a") as file:
            file.truncate(0)
    file.close
    for i in range(args.n):
        time.sleep(args.i)
        with open(args.f, "a") as file:
            json_object = json.dumps(util.make_snapshot())
            file.write(json_object + "\n")
        os.system("clear")
        print(util.make_snapshot(), end="\r")
        file.close()



if __name__ == "__main__":
    main()
